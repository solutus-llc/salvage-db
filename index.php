<?php
ini_set( 'max_execution_time' , 0);
require_once dirname(__FILE__) . '/vendor/autoload.php';

$compress = true;
$dbFile = ''; // File name to import
$dbName = '';
$host = 'localhost';
$pass = '';
$port = '3306';
$type = 'export'; // or import
$user = '';

if (isset($_POST['host'])) {
    $host = $_POST['host'];
}
if (isset($_POST['port'])) {
    $port = $_POST['port'];
}
if (isset($_POST['dbName'])) {
    $dbName = $_POST['dbName'];
}
if (isset($_POST['databaseName'])) {
    $dbName = $_POST['databaseName'];
}
if (isset($_POST['user'])) {
    $user = $_POST['user'];
}
if (isset($_POST['pass'])) {
    $pass = $_POST['pass'];
}

if (isset($_POST['type'])) {
    $type = $_POST['type'];
}

if (isset($_POST['dbFile'])) {
    $dbFile = $_POST['dbFile'];
}

if (isset($_POST['compress'])) {
    $compress = ((bool) $_POST['compress']);
}

$dsn = sprintf(
    'mysql:host=%s;port=%s;dbname=%s',
    $host,
    $port,
    $dbName
);

if( 'export' == $type ) {

    // https://github.com/eaudeweb/mysqldump-php#constructor-and-default-parameters
    $dumpSettings = array(
        'compress' => $compress ? Ifsnop\Mysqldump\Mysqldump::GZIP : Ifsnop\Mysqldump\Mysqldump::NONE,
        'add-locks' => true,
        'single-transaction' => true
    );
    
    try {
        $dump = new Ifsnop\Mysqldump\Mysqldump($dsn, $user, $pass, $dumpSettings);
        $dump->start($dbName . '.sql' . ($compress ? '.gz' : ''));
    } catch (\Exception $e) {
        echo 'Error occurred. Please contact https://gitlab.com/solutus-llc/salvage-db/-/issues';
    }

} elseif('import' == $type) {
    
    if( 'gz' == pathinfo($dbFile, PATHINFO_EXTENSION) ){
        $fp = gzopen($dbFile, 'r');
        $SQL_CONTENT = gzdecode(file_get_contents($dbFile)); 
    }elseif( 'sql' == pathinfo($dbFile, PATHINFO_EXTENSION) ){
        $fp = fopen($dbFile, 'r');
        $SQL_CONTENT = file_get_contents($dbFile); 
    }else{
        echo "Error: This file is not an SQL file.";
        return -1;
    }

    // Connect to MySQL server
    $link = mysqli_connect($host, $user, $pass, $dbName) or die('Error connecting to MySQL server: ' . mysqli_connect_error());
    
    // Temporary variable, used to store current query
    $templine = '';
    if ( $fp && $SQL_CONTENT && $link ) {
        preg_match_all("/(\s+)CREATE TABLE(\s+)\`(.+)\`/i", $SQL_CONTENT, $target_tables); 
    
        mysqli_query($link, 'SET foreign_key_checks = 0');
		foreach ( $target_tables[3] as $table )
		{
            mysqli_query($link, 'DROP TABLE IF EXISTS '.$table);
        }         
        mysqli_query($link, 'SET foreign_key_checks = 1');
        
        while (!feof($fp)) { // Loop through each line
            $line = trim(fgets($fp));
            // Skip it if it's a comment
            if (substr($line, 0, 2) == '--' || $line == '') {
                continue;
            }

            // Add this line to the current segment
            $templine .= $line;

            // If it has a semicolon at the end, it's the end of the query
            if (substr(trim($line), -1, 1) == ';') {
                // Perform the query
                mysqli_query($link, $templine) or print('Error performing query "' . $templine . '":' . mysqli_error($link) . PHP_EOL);
                // Reset temp variable to empty
                $templine = '';
            }
        }
    }
    fclose($fp);
    echo "Tables imported successfully";
}
